// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyANckhEos2JyINIQd7wRYKKZBvy_5jpvuY",
        authDomain: "iptv-master-d4a75.firebaseapp.com",
        databaseURL: "https://iptv-master-d4a75.firebaseio.com",
        projectId: "iptv-master-d4a75",
        storageBucket: "iptv-master-d4a75.appspot.com",
        messagingSenderId: "978014911247"
    }
};
