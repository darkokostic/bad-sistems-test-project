import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ProjectsListComponent} from './projects/projects-list/projects-list.component';
import {ProjectSingleComponent} from './projects/project-single/project-single.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { environment } from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {ProjectsService} from './projects/shared/projects.service';

@NgModule({
    declarations: [
        AppComponent,
        ProjectsListComponent,
        ProjectSingleComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule
    ],
    providers: [ProjectsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
