import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectsListComponent} from '../projects/projects-list/projects-list.component';
import {ProjectSingleComponent} from '../projects/project-single/project-single.component';

const routes: Routes = [
    { path: '', redirectTo: '/projects-list', pathMatch: 'full' },
    { path: 'projects-list', component: ProjectsListComponent },
    { path: 'projects-list/:id', component: ProjectSingleComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {
}
