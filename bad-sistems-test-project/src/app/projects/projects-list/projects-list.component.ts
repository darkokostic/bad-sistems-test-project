import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../shared/projects.service';
import {Project} from '../shared/project.model';

@Component({
    selector: 'app-projects',
    templateUrl: './projects-list.component.html',
    styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

    projects: Project[];

    constructor(private projectsService: ProjectsService) {}

    ngOnInit() {
        this.projectsService.getData()
            .snapshotChanges()
            .subscribe(projects => {
                this.projects = [];
                projects.forEach(project => {
                    const currentProject = project.payload.toJSON();
                    currentProject['$key'] = project.key;
                    this.projects.push(currentProject as Project);
                });
            });
    }
}
