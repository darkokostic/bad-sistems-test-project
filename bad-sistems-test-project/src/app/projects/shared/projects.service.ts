import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

@Injectable()
export class ProjectsService {

    private projectsList: AngularFireList<any>;

    constructor(private firebase: AngularFireDatabase) {}

    getData() {
        this.projectsList = this.firebase.list('projects');
        return this.projectsList;
    }

    getSingleProject(id) {
        return this.firebase.object('/projects/' + id);
    }

    getLogs(id) {
        return this.firebase.list('/logs/' + id);
    }
}
