import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProjectsService} from '../shared/projects.service';
import {Project} from '../shared/project.model';
import {Log} from '../shared/log.model';

@Component({
    selector: 'app-project-single',
    templateUrl: './project-single.component.html',
    styleUrls: ['./project-single.component.css']
})
export class ProjectSingleComponent implements OnInit {

    projectId: string;
    project: Project;
    logs: Log[];

    constructor(private route: ActivatedRoute, private projectsService: ProjectsService) {
        this.route.params.subscribe(params => this.projectId = params.id);
    }

    ngOnInit() {
        this.projectsService.getSingleProject(this.projectId)
            .snapshotChanges()
            .subscribe(project => {
                this.project = project.payload.toJSON() as Project;
            });

        this.projectsService.getLogs(this.projectId)
            .snapshotChanges()
            .subscribe(items => {
                this.logs = [];
                items.forEach(log => {
                    this.logs.push(log.payload.toJSON() as Log);
                });
            });
    }

}
